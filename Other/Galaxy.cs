﻿using UnityEngine;
using System.Collections;

public class Galaxy : MonoBehaviour {

	public GameObject galaxy01;
	public GameObject galaxy02;
	public GameObject galaxy03;
	public GameObject galaxy04;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		galaxy01.transform.Rotate(Vector3.up * (2.0f * Time.deltaTime));
		galaxy02.transform.Rotate(-Vector3.up * (2.5f * Time.deltaTime));
		galaxy03.transform.Rotate(Vector3.up * (3.0f * Time.deltaTime));
		galaxy04.transform.Rotate(-Vector3.up * (2.0f * Time.deltaTime));
	}
}
