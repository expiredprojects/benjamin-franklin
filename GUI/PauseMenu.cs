﻿using UnityEngine;
using System.Collections;

public class PauseMenu : MonoBehaviour {
	public GameObject blackPlane;

	public GameObject pauseButton;
	public GameObject retryButton;
	public GameObject mainMenuButton;

	public void Show() {
		iTween.FadeTo(blackPlane, 0.5f, 0.3f);

		Hashtable retryHT = new Hashtable();
		retryHT.Add("position",retryButton.GetComponent<RetryButton>().finalPosition);
		retryHT.Add("time",0.6f);
		retryHT.Add("delay",0.15f);
		retryHT.Add("easetype",iTween.EaseType.easeOutElastic);
		iTween.MoveTo(retryButton, retryHT);

		Hashtable mainMenuHT = new Hashtable();
		mainMenuHT.Add("position",mainMenuButton.GetComponent<MainMenuButton>().finalPosition);
		mainMenuHT.Add("time",0.6f);
		mainMenuHT.Add("delay",0.3f);
		mainMenuHT.Add("easetype",iTween.EaseType.easeOutElastic);
		iTween.MoveTo(mainMenuButton, mainMenuHT);
	}

	public void Hide(bool goToMain) {
		pauseButton.GetComponent<MenuButton>().isBackButton = false;
		pauseButton.GetComponent<TextMesh>().text = "PAUSE";

		if (goToMain) {
			iTween.FadeTo(blackPlane, 1, 1);
		}
		else {
			iTween.FadeTo(blackPlane, 0.0f, 0.3f);
		}

		Hashtable retryHT = new Hashtable();
		retryHT.Add("position",retryButton.GetComponent<RetryButton>().initPosition);
		retryHT.Add("time",0.2f);
		retryHT.Add("delay",0.2f);
		retryHT.Add("easetype",iTween.EaseType.easeInBack);
		iTween.MoveTo(retryButton, retryHT);

		Hashtable mainMenuHT = new Hashtable();
		mainMenuHT.Add("position",mainMenuButton.GetComponent<MainMenuButton>().initPosition);
		mainMenuHT.Add("time",0.2f);
		mainMenuHT.Add("delay",0.15f);
		mainMenuHT.Add("easetype",iTween.EaseType.easeInBack);
		iTween.MoveTo(mainMenuButton, mainMenuHT);
	}
}
