﻿using UnityEngine;
using System.Collections;

public class Score : MonoBehaviour {

	public void SetScore (int score) {
		gameObject.GetComponent<TextMesh>().text = score.ToString();
	}
}
