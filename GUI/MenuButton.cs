﻿using UnityEngine;
using System.Collections;

public class MenuButton : MonoBehaviour {
	public PauseMenu menu;
	public LevelLogic levelLogic;

	private bool buttonDown;
	private bool pressed;
	public bool isBackButton;
	
	void Start () {
		buttonDown = false;
		pressed = false;
		isBackButton = false;

		gameObject.GetComponent<TextMesh>().text = "PAUSE";
	}
	
	void Update () {
		if (pressed && !isBackButton) {
			gameObject.GetComponent<TextMesh>().text = "RESUME";
			menu.Show();
			levelLogic.Pause();
			isBackButton = true;
			pressed = false;
		}
		else if (pressed && isBackButton) {
			gameObject.GetComponent<TextMesh>().text = "PAUSE";
			menu.Hide(false);
			levelLogic.Resume();
			isBackButton = false;
			pressed = false;
		}
	}
	
	void OnMouseDown()
	{
		buttonDown = true;
		iTween.ScaleTo(gameObject, new Vector3(0.95f, 0.95f, 0.95f), 0.1f);
		iTween.FadeTo(gameObject, 0.6f, 0.1f);
	}
	
	void OnMouseUpAsButton()
	{
		if (buttonDown) {
			buttonDown = false;
			iTween.ScaleTo(gameObject, new Vector3(1, 1, 1), 0.2f);
			iTween.FadeTo(gameObject, 1, 0.2f);

			gameObject.GetComponent<AudioSource>().Play();
			
			pressed = true;
		}
	}
	
	void OnMouseExit () {
		buttonDown = false;
		iTween.ScaleTo(gameObject, new Vector3(1, 1, 1), 0.2f);
		iTween.FadeTo(gameObject, 1, 0.2f);
	}
}
