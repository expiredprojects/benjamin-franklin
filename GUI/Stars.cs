﻿using UnityEngine;
using System.Collections;

public class Stars : MonoBehaviour {

	public StarIndicator[] stars;
	
	public void UpdateStars (int starNum) {
		for (int i=0; i<starNum && i<stars.Length; i++) {
			stars[i].LightUp();
		}
	}

	public void Reset() {
		for (int i=0; i<stars.Length; i++) {
			stars[i].TurnOff();
		}
	}
}
