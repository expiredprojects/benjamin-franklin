﻿using UnityEngine;
using System.Collections;

public class RetryButton : MonoBehaviour {
	public PauseMenu menu;
	public LevelLogic levelLogic;
	
	public Vector3 initPosition;
	public Vector3 finalPosition;
	
	private bool buttonDown;
	private bool pressed;

	void Awake () {
		initPosition = new Vector3(-11.0f, 0.5f, -5.5f);
	}

	void Start () {
		buttonDown = false;
		pressed = false;

		finalPosition = transform.position;
		transform.position = initPosition;
	}
	
	void Update () {
		if (pressed) {
			levelLogic.LevelReset();
			menu.Hide(false);
			pressed = false;
		}
	}
	
	void OnMouseDown()
	{
		buttonDown = true;
		iTween.ScaleTo(gameObject, new Vector3(0.95f, 0.95f, 0.95f), 0.1f);
		iTween.FadeTo(gameObject, 0.6f, 0.1f);
	}
	
	void OnMouseUpAsButton()
	{
		if (buttonDown) {
			buttonDown = false;
			iTween.ScaleTo(gameObject, new Vector3(1, 1, 1), 0.2f);
			iTween.FadeTo(gameObject, 1, 0.2f);
			
			gameObject.GetComponent<AudioSource>().Play();
			
			pressed = true;
		}
	}
	
	void OnMouseExit () {
		buttonDown = false;
		iTween.ScaleTo(gameObject, new Vector3(1, 1, 1), 0.2f);
		iTween.FadeTo(gameObject, 1, 0.2f);
	}
}
