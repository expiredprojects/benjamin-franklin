﻿using UnityEngine;
using System.Collections;

public class SummaryRetryButton : MonoBehaviour {
	public Summary summary;
	public LevelLogic levelLogic;
	
	private bool buttonDown;
	private bool pressed;
	
	void Start () {
		buttonDown = false;
		pressed = false;
	}
	
	void Update () {
		if (pressed) {
			levelLogic.LevelReset();
			summary.Hide(false);
			pressed = false;
		}
	}
	
	void OnMouseDown()
	{
		buttonDown = true;
		iTween.ScaleTo(gameObject, new Vector3(0.95f, 0.95f, 0.95f), 0.1f);
		iTween.FadeTo(gameObject, 0.6f, 0.1f);
	}
	
	void OnMouseUpAsButton()
	{
		if (buttonDown) {
			buttonDown = false;
			iTween.ScaleTo(gameObject, new Vector3(1, 1, 1), 0.2f);
			iTween.FadeTo(gameObject, 1, 0.2f);
			
			gameObject.GetComponent<AudioSource>().Play();
			
			pressed = true;
		}
	}
	
	void OnMouseExit () {
		buttonDown = false;
		iTween.ScaleTo(gameObject, new Vector3(1, 1, 1), 0.2f);
		iTween.FadeTo(gameObject, 1, 0.2f);
	}
}
