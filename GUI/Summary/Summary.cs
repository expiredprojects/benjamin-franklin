﻿using UnityEngine;
using System.Collections;

public class Summary : MonoBehaviour {
	public LevelLogic levelLogic;
	public GameObject pauseButton;

	public GameObject blackPlane;
	
	public GameObject retryButton;
	public GameObject menuButton;
	public GameObject nextButton;

	public Stars stars;
	public TextMesh takeOffQuantityText;
	public TextMesh timeQuantityText;

	public TextMesh starScoreText;
	public TextMesh takeOffScoreText;
	public TextMesh timeScoreText;
	public TextMesh totalScoreText;
	public TextMesh highScoreText;

	public Vector3 initPosition;
	public Vector3 finalPosition;
	
	private int starScore;
	private int takeOffScore;
	private int timeScore;
	private int score;
	private int highscore;

	private float time;
	private float delay;
	private float countDelay;
	private bool isShown;
	private bool delayPassed;
	private int scoreCounter;

	private bool starScoreCounted;
	private bool takeOffScoreCounted;
	private bool timeScoreCounted;
	private bool totalScoreCounted;
	private bool highScoreCounted;

	void Awake () {
		initPosition = new Vector3(0, 8, -6.0f);
	}

	void Start() {
		time = 0;
		delay = 1.0f;
		countDelay = 0.001f;
		delayPassed = false;

		isShown = false;

		starScoreCounted = false;
		takeOffScoreCounted = false;
		timeScoreCounted = false;
		totalScoreCounted = false;
		highScoreCounted = false;

		highscore = PlayerPrefs.GetInt(levelLogic.levelName + "_highscore");
		score = 0;

		finalPosition = new Vector3(0, 0, -6.0f);
		transform.position = initPosition;
	}
	
	void Update() {
		if (isShown) {
			time += Time.deltaTime;

			if (time >= delay && !delayPassed) {
				delayPassed = true;
				time = 0;
			}
			else if (delayPassed) {
				if (!starScoreCounted) {
					if (scoreCounter >= starScore) {
						starScoreText.text = starScore.ToString();
						scoreCounter = 0;
						starScoreCounted = true;
					}
					else if (time >= countDelay) {
						starScoreText.text = (scoreCounter+=5).ToString();
						time = 0;
					}
				}
				else if (!takeOffScoreCounted) {
					if (scoreCounter >= takeOffScore) {
						takeOffScoreText.text = takeOffScore.ToString();
						scoreCounter = 0;
						takeOffScoreCounted = true;
					}
					else if (time >= countDelay) {
						takeOffScoreText.text = (scoreCounter+=5).ToString();
						time = 0;
					}
				}
				else if (!timeScoreCounted) {
					if (scoreCounter >= timeScore) {
						timeScoreText.text = timeScore.ToString();
						scoreCounter = 0;
						timeScoreCounted = true;
					}
					else if (time >= countDelay) {
						timeScoreText.text = (scoreCounter+=5).ToString();
						time = 0;
					}
				}
				else if (!totalScoreCounted) {
					if (scoreCounter >= score) {
						starScoreText.text = score.ToString();
						scoreCounter = 0;
						totalScoreCounted = true;
					}
					else if (time >= countDelay) {
						totalScoreText.text = (scoreCounter+=10).ToString();
						time = 0;
					}
				}
				else if (!highScoreCounted) {
					if (scoreCounter >= highscore) {
						highScoreText.text = highscore.ToString();
						scoreCounter = 0;
						highScoreCounted = true;
					}
					else if (time >= countDelay) {
						highScoreText.text = (scoreCounter+=20).ToString();
						time = 0;
					}
				}
			}
		}
	}

	public void Show() {
		Debug.Log("Highscore: " + highscore);
		if (score > highscore) {
			highscore = score;
			PlayerPrefs.SetInt(levelLogic.levelName + "_highscore", highscore);
		}

		scoreCounter = 0;

		stars.UpdateStars(levelLogic.stars);
		takeOffQuantityText.text = (levelLogic.takeOffs).ToString();
		timeQuantityText.text = (levelLogic.time).ToString();
		timeQuantityText.text = timeQuantityText.text.Replace(".", ":");
		starScore = 0;
		takeOffScore = 200;
		timeScore = 300;
		score = 900;
		highscore = 1900;

		starScoreText.text = "0";
		takeOffScoreText.text = "0";
		timeScoreText.text = "0";
		totalScoreText.text = "0";
		highScoreText.text = "0";

		Hashtable pauseHT = new Hashtable();
		pauseHT.Add("position",new Vector3(-11.0f, 4.44f, -5.5f));
		pauseHT.Add("time",0.2f);
		pauseHT.Add("easetype",iTween.EaseType.easeInBack);
		iTween.MoveTo(pauseButton, pauseHT);

		Hashtable blackHT = new Hashtable();
		blackHT.Add("alpha",0.5f);
		blackHT.Add("time",0.3f);
		blackHT.Add("delay",0.2f);
		iTween.FadeTo(blackPlane, blackHT);
		
		Hashtable summaryHT = new Hashtable();
		summaryHT.Add("position",finalPosition);
		summaryHT.Add("time",0.6f);
		summaryHT.Add("delay",0.2f);
		summaryHT.Add("easetype",iTween.EaseType.easeOutElastic);
		iTween.MoveTo(gameObject, summaryHT);

		isShown = true;
	}

	public void Hide(bool fadeCompletely) {
		
		if (fadeCompletely) {
			iTween.FadeTo(blackPlane, 1, 1);
		}
		else {
			Hashtable pauseHT = new Hashtable();
			pauseHT.Add("position",new Vector3(-7.21f, 4.44f, -5.5f));
			pauseHT.Add("time",0.6f);
			pauseHT.Add("delay",0.5f);
			pauseHT.Add("easetype",iTween.EaseType.easeOutElastic);
			iTween.MoveTo(pauseButton, pauseHT);

			iTween.FadeTo(blackPlane, 0.0f, 0.3f);
		}

		Hashtable summaryHT = new Hashtable();
		summaryHT.Add("position",initPosition);
		summaryHT.Add("time",0.2f);
		summaryHT.Add("easetype",iTween.EaseType.easeInBack);
		iTween.MoveTo(gameObject, summaryHT);
	}
}
