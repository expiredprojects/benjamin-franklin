﻿using UnityEngine;
using System.Collections;

public class StarIndicator : MonoBehaviour {
	public Sprite[] starSprites;

	void Start () {
		gameObject.GetComponent<SpriteRenderer>().sprite = starSprites[0];
	}
	
	public void LightUp () {
		gameObject.GetComponent<SpriteRenderer>().sprite = starSprites[1];
	}

	public void TurnOff () {
		gameObject.GetComponent<SpriteRenderer>().sprite = starSprites[0];
	}
}
