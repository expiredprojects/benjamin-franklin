﻿using UnityEngine;
using System.Collections;

public class SpikyPlanet : Planet {

	void OnCollisionEnter2D(Collision2D c) {
		Hero hero = c.gameObject.GetComponent<Hero>();
		hero.isDead = true;
	}
}
