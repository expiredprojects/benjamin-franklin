﻿using UnityEngine;
using System.Collections;

public class Planet : MonoBehaviour {

	//Config
	private SpriteRenderer spriteRenderer;
	private float initialAlpha;

	void Awake() {
		spriteRenderer = this.GetComponentInChildren<Gravity>().GetComponent<SpriteRenderer>();
		initialAlpha = spriteRenderer.color.a;
	}

	public void toggleGravity(bool boolean) {
		if (boolean) spriteRenderer.color = new Color(spriteRenderer.color.r, spriteRenderer.color.g, spriteRenderer.color.b, 0.3f);
		else spriteRenderer.color = new Color(spriteRenderer.color.r, spriteRenderer.color.g, spriteRenderer.color.b, initialAlpha);
	}

	void OnCollisionEnter2D(Collision2D c) {
		if (c.gameObject.name==Hero.heroName) {
			Hero hero = c.gameObject.GetComponent<Hero>();
			hero.time = 0f;
			hero.isLocked = true;
			hero.isGrounded = true;
			hero.isDefying = false;
			toggleGravity(hero.isDefying);
			hero.rigidbody2D.velocity = Vector2.zero;
			hero.rigidbody2D.angularVelocity = 0f;
		}
	}

	void OnCollisionStay2D(Collision2D c) {
		if (c.gameObject.name==Hero.heroName) {
			Hero hero = c.gameObject.GetComponent<Hero>();
			if (!hero.isDefying) {
				float tx = this.transform.position.x;
				float ty = this.transform.position.y;
				float cx = c.transform.position.x;
				float cy = c.transform.position.y;
				Vector2 localGravity = new Vector2(tx - cx, ty - cy);
				c.gameObject.rigidbody2D.velocity = localGravity;
			}
		}
	}

	void OnCollisionExit2D(Collision2D c) {
		if (c.gameObject.name==Hero.heroName) {
			Hero hero = c.gameObject.GetComponent<Hero>();
			if (hero.isDefying) {
				Debug.Log("Exit");
				hero.isGrounded = false;
			}
		}
	}
}
