﻿using UnityEngine;
using System.Collections;

public class Hero : MonoBehaviour {

	//Debug
	private SpriteRenderer spriteRenderer;
	public float current;

	//Editor
	public Vector2 initialVelocity = Vector2.zero;
	public float planetSpeed = 50f;
	public float jumpSpeed = 50f;
	public float limitLock = 0.2f;

	//State
	public bool isGrounded;
	public bool isDefying;
	private bool isChanging;
	public bool isLocked;
	public bool isDead;
	
	//Config
	public static string heroName;
	private ArrowPointer arrowPointer;
	[HideInInspector]
	public Vector2 initialPosition;
	[HideInInspector]
	public float time;
	
	void Awake() {
		heroName = this.gameObject.name;
		isGrounded = false;
		isDefying = false;
		isChanging = false;
		isLocked = false;
		isDead = false;
		spriteRenderer = this.GetComponent<SpriteRenderer>();
		arrowPointer = this.GetComponentInChildren<ArrowPointer>();
	}
	
	void Start() {
		initialPosition = transform.position;
		Init();
	}

	public void Init() {
		this.transform.position = initialPosition;
		rigidbody2D.rotation = 0f;
		rigidbody2D.velocity = initialVelocity;
		DebugColor();
//		arrowPointer.setVisible(false);
	}

	void OnCollisionStay2D(Collision2D c) {
		float tx = this.transform.position.x;
		float ty = this.transform.position.y;
		float cx = c.transform.position.x;
		float cy = c.transform.position.y;
		Vector2 localGravity = new Vector2(cx - tx, cy - ty);

		if (!arrowPointer.isVisible()) arrowPointer.setVisible(true);
		float magicRotation = Mathf.Acos((ty - cy) / (Mathf.Sqrt(Mathf.Pow(tx - cx, 2)+Mathf.Pow(ty - cy, 2)))) * Mathf.Rad2Deg;

		this.rigidbody2D.rotation = (tx - cx > 0) ? -1 * magicRotation : magicRotation;
		//Debug.DrawLine(this.transform.position, c.transform.position);
		//Debug.Log(this.transform.position + " and " + c.transform.position);

		if (isGrounded && Input.GetKey(KeyCode.LeftArrow)) {
			rigidbody2D.AddForce(new Vector2(localGravity.y * planetSpeed, -localGravity.x * planetSpeed));
		}
		else if (isGrounded && Input.GetKey(KeyCode.RightArrow)) {
			rigidbody2D.AddForce(new Vector2(-localGravity.y * planetSpeed, localGravity.x * planetSpeed));
		}
		else if (!isLocked && isGrounded && Input.GetKeyDown(KeyCode.Space)) {
			isChanging = true;
			isDefying = true;
			DebugColor();
			rigidbody2D.AddForce(new Vector2(-localGravity.x * jumpSpeed, -localGravity.y * jumpSpeed));
			arrowPointer.setVisible(false);
		}
	}

	void Update() {
		if (isLocked) {
			time += Time.deltaTime;
			if (time > limitLock) {
				isLocked = false;
			}
		} else {
			if (isChanging) isChanging = false;
			else if (!isGrounded && Input.GetKeyDown(KeyCode.Space)) {
				isDefying = !isDefying;
				DebugColor();
			}
		}

		if (!isDead) {
			if (!spriteRenderer.isVisible) isDead = true;
		}
	}

	public void DebugColor() {
		Planet[] planets = FindObjectsOfType<Planet>();
		foreach (Planet p in planets) {
			p.toggleGravity(isDefying);
		}
	}
}
