﻿using UnityEngine;
using System.Collections;

public class Gravity : MonoBehaviour {

	//Editor
	public float forceFactor = 5f;

	private float radius;

	void Awake() {
		radius = this.GetComponent<CircleCollider2D>().bounds.size.x / 2;
	}

	void OnTriggerStay2D(Collider2D c) {
		if (c.gameObject.name==Hero.heroName) {
			Hero hero = c.gameObject.GetComponent<Hero>();
			if (!hero.isGrounded&&!hero.isDefying) {
				float tx = this.transform.position.x;
				float ty = this.transform.position.y;
				float cx = c.transform.position.x;
				float cy = c.transform.position.y;
				float distance = Mathf.Sqrt(Mathf.Pow(tx - cx, 2) + Mathf.Pow(ty - cy, 2));
				float force = forceFactor * (1 - Mathf.Clamp(Mathf.Pow(distance / radius, 2), 0, 1));
				//Debug.Log ("Radius: " + radius + " Dist: " + distance);
				Vector2 vector = new Vector2((tx - cx) * force , (ty - cy) * force);
				c.gameObject.rigidbody2D.AddForce(vector);
				//Debug.Log(" Force: " + force + "Vector: " + vector);
			}
		}
	}
}
