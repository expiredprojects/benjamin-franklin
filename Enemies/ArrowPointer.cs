﻿using UnityEngine;
using System.Collections;

public class ArrowPointer : MonoBehaviour {	
	private SpriteRenderer spriteRenderer;

	void Awake() {
		spriteRenderer = this.GetComponent<SpriteRenderer>();
		spriteRenderer.enabled = false;
	}

	public bool isVisible() {
		return spriteRenderer.enabled;
	}

	public void setVisible(bool boolean) {
		//Debug.Log ("Party time: " + Time.time + " Result: " + boolean);
		spriteRenderer.enabled = boolean;
	}
}
