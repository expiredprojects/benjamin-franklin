﻿using UnityEngine;
using System.Collections;

public class LevelLogic : MonoBehaviour {
	public string levelName;

	public Hero hero;
	public Stars starsIndicator;

	public int stars;
	public int takeOffs;
	public float time;

	public GameObject summary;

	private Vector3 heroSpawnPos;
	private bool pause;
	private bool gameOver;

	void Awake() {
		hero = FindObjectOfType<Hero>();
	}

	void Start () {
//		LevelReset();
	}

	void Update () {
		if (!pause && !gameOver) {
			time += Time.deltaTime;
		}

		if (hero.isDead) {
			hero.isDead = false;
			Respawn();
		}
	}

	public void Pause() {
		pause = true;
	}

	public void Resume() {
		pause = false;
	}

	public void AddStar() {
		starsIndicator.UpdateStars(++stars);
	}

	public void Respawn() {
		hero.Init();
	}

	public void LevelReset() {
		gameObject.transform.position = new Vector3(0,0,0);
		stars = 0;
		takeOffs = 0;
		time = 0;
		
		pause = false;
		gameOver = false;

		starsIndicator.Reset();
	}

	public void GameOver() {
		gameOver = true;
		summary.GetComponent<Summary>().Show();
	}
}
