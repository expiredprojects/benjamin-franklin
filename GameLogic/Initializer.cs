﻿using UnityEngine;
using System.Collections;

public class Initializer : MonoBehaviour {
	public GameObject blackPlane;

	public GameObject titleScreen;
	public MenuManager menuManager;
	public LevelManager levelManager;

	public LevelLogic level;

	public Vector3 titleScreenInitPosition;
	public Vector3 titleScreenFinalPosition;

	void Start () {
		titleScreenInitPosition = new Vector3(0,0,0);
		titleScreenFinalPosition = new Vector3(-200,0,0);
		ShowTitle();
	}
	
	void Update () {
	
	}

	private void ShowTitle() {
		blackPlane.renderer.material.color = new Vector4(1, 1, 1, 1);
		iTween.FadeTo(blackPlane, 0.0f, 2);
		if (titleScreen.GetComponent<AudioSource>() != null) {
			titleScreen.GetComponent<AudioSource>().Play();
		}
	}

	public void StartGame() {
//		iTween.FadeTo(blackPlane, 1, 1);
		level.LevelReset();
		titleScreen.transform.position = new Vector3(0,0,0);
	}
}
