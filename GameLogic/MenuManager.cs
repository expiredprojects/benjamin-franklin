﻿using UnityEngine;
using System.Collections;

public class MenuManager : MonoBehaviour {

	public LevelManager levelManager;
	public GameObject blackPlane;

	void Start () {
	
	}

	void Update () {
	
	}

	public void GoToMainMenu(GameObject pauseMenu) {
		if (pauseMenu != null) {
			Hashtable pauseHT = new Hashtable();
			pauseHT.Add("position", new Vector3(-11.0f, -0.5f, -5.5f));
			pauseHT.Add("time",0.2f);
			pauseHT.Add("delay",0.25f);
			pauseHT.Add("easetype",iTween.EaseType.easeInBack);
			iTween.MoveTo(pauseMenu, pauseHT);
		}
	}

	public void GoToTitle() {

	}
}
